import java.util.Scanner
import kotlin.math.*
 //Por que al poner una variable, si se pone en mayusculas la primer letra, se pone en verde?
fun main(){

    do{
        with(Scanner(System. `in`)){

        println("Ingresa los numeros separados por comas o 'exit' para salir:")
        //readLine() para leer lo que se escribe en la linea
        //.split es para la separacion por medio de comas
        //.map para que los valores los tome como enteros y no texto
        //it.toInt() es para tomar los enteros
        //SPLIT ES PARA TOMAR LOS NUMERO CON LA SEPARACION EN EL PARENTESIS
        try{
        var datos = readLine()!!
        if(datos == ""){ //{{ }} →
            println("Please insert integer numeric values separated by commas.")     
        }
        else if(datos == " "){
            println("Please insert integer numeric values separated by commas.")
        }
        else if(datos == "exit" ){
            System.exit(0)
        }
        else{    
        var numIngresados = datos.split(",").map {it.toInt()} //Each value from the population
        var promedio = numIngresados.average() //.average() obtiene el promedio de la lista de valores ingresados de la variable
        var cantidadValores = numIngresados.count() //Contara cada numero o par de numero ingresado
        var sumatoria = 0.0
        

        for (i in numIngresados){
            var x = Math.pow(i - promedio,2.0)
            sumatoria += x
        }
            var desviacion = Math.sqrt(sumatoria/cantidadValores)

            println("La desviacion estandar es de: %.4f".format(desviacion))
        }

    }catch(e: NumberFormatException){
    println("Some value is wrong. Please insert integer numeric values separated by commas.")
        } 
}
}while(true)
}



